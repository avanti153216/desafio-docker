resource "aws_instance" "desafio" {
    ami = "ami-04b70fa74e45c3917"
    instance_type = "t2.micro"
    key_name = "Avanti" //verificar se existe o key pair criado na AWS
    security_groups = ["allow_ssh", "allow_http", "allow_egress", "allow_https"]
    user_data = file("script.sh")
    tags = {
        Name = "desafio"
    }
  
}