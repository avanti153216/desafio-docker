#!/bin/bash

sudo apt-get update
sudo apt-get upgrade -y

# Install Docker
 curl -fsSL https://get.docker.com -o get-docker.sh
 sudo sh get-docker.sh

 # Install Gitlab Runner
 curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
 sudo apt-get install gitlab-runner
 sudo usermod -aG docker gitlab-runner

# Register Gitlab Runner

sudo gitlab-runner register \
  --non-interactive \
  --url https://gitlab.com/ \
  --token glrt-ywYUB4btXJVZLTz5ywsJ \
  --executor "shell" \



